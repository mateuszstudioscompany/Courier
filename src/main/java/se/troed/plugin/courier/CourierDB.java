package se.troed.plugin.courier;

import com.google.common.io.Files;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

/**
 * Flatfile now, database later
 * I'm quite sure I could get rid of messageids by using other primitives 
 * "delivered" and "read" are slightly tricky. Delivered mail sets newmail to false, even when not read.
 * (and of course delivered=false and read=true is an invalid combination should it arise)
 *
 * courierclaimedmap: mapid       # 17 chars = cannot clash with any playername
 * receiver1:
 *   newmail: true/false          <-- makes some things faster but others slow
 *   messageids: 42,73,65         <-- get/setIntegerList, although it currently doesn't look this pretty in the yml
 *   mapid42:
 *     sender:
 *     message:
 *     date:
 *     delivered:
 *     read:
 *   mapid73:
 *     sender:
 *     message:
 *     date:
 *     delivered:
 *     read:
 *   mapid65:
 *     sender:
 *     message:
 *     date:
 *     delivered:
 *     read:
 * receiver2:
 *   ...
 *
 */
public class CourierDB {
    private static final String FILENAME = "messages.yml";
    public static final String KEY_MESSAGEID_SUFFIX = ".messageids";
    public static final String KEY_MESSAGE_SUFFIX = ".message";
    public static final String KEY_SENDER_SUFFIX = ".sender";
    public static final String KEY_DATE_SUFFIX = ".date";
    public static final String KEY_DELIVERED_SUFFIX = ".delivered";
    public static final String KEY_NEWMAIL_SUFFIX = ".newmail";
    public static final String KEY_READ_SUFFIX = ".read";
    private final Courier plugin;
    private YamlConfiguration mdb;
    
    CourierDB(Courier p) {
        plugin = p;    
    }

    // reading the whole message db into memory, is that a real problem?
    // returns true if there already was a db
    boolean load() throws IOException {
        File db = new File(plugin.getDataFolder(), FILENAME);
        mdb = new YamlConfiguration();
        if (db.exists()) {
            try {
                mdb.load(db);
            } catch (InvalidConfigurationException e) {
                // this might be a MacRoman (or other) encoded file and we're running under a UTF-8 default JVM
                mdb = loadNonUTFConfig(db);
                if(mdb == null) {
                    throw new IOException("Could not read Courier database!");
                }
            } catch (Exception e) {
                mdb = null;
                // e.printStackTrace();
                throw new IOException("Could not read Courier database!");
            }
            return true;
        }
        return false;
    }

    // see http://forums.bukkit.org/threads/friends-dont-let-friends-use-yamlconfiguration-loadconfiguration.57693/
    // manually load as MacRoman if possible, we'll force saving in UTF-8 later
    // Testing shows Apple Java 6 with default-encoding utf8 finds a "MacRoman" charset
    // OpenJDK7 on Mac finds a "x-MacRoman" charset.
    //
    // "Every implementation of the Java platform is required to support the following standard charsets. Consult the release documentation for your implementation to see if any other charsets are supported. The behavior of such optional charsets may differ between implementations.
    // US-ASCII, ISO-8859-1, UTF-8 [...]"
    //
    // http://www.alanwood.net/demos/charsetdiffs.html - compares ansi, iso and macroman
    // NOTE: This method isn't pretty and should - really - be recoded.
    private YamlConfiguration loadNonUTFConfig(File db) {
        Charset cs;
        try {
            // This issue SHOULD be most common on Mac, I think, assume MacRoman default
            cs = Charset.forName("MacRoman");
        } catch (Exception e) {
            cs = StandardCharsets.ISO_8859_1;
        }

        InputStreamReader reader; 
        try {
            plugin.getCConfig().clog(Level.WARNING, "Trying to convert message database from " + cs.displayName() + " to UTF-8");
            reader = new InputStreamReader(new FileInputStream(db), cs);
        } catch (Exception e) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        try (BufferedReader input = new BufferedReader(reader)) {
            String line;

            while ((line = input.readLine()) != null) {
                builder.append(line);
                builder.append('\n');
            }
        } catch (Exception e) {
            return null;
        }

        mdb = new YamlConfiguration();
        try {
            mdb.loadFromString(builder.toString());
        } catch (Exception e) {
            mdb = null;
            e.printStackTrace();
        }
        return mdb;
    }

    // if filename == null, uses default
    // (this makes taking backups really easy)
    void save(String filename) {
        if (mdb != null) {
            File db = new File(plugin.getDataFolder(), filename != null ? filename : FILENAME);
            try {
                // saveUTFConfig(db, mdb);
                mdb.save(db);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // even if we're run under a JVM with non-utf8 default encoding, force it
    // at least that was the idea, but on Mac it's still read back using MacRoman. No automatic switching to UTF-8
    public void saveUTFConfig(File file, YamlConfiguration yaml) throws IOException {
        if (yaml != null) {
            Charset cs;
            try {
                cs = StandardCharsets.UTF_8;
            } catch (Exception e) {
                throw new IOException("UTF-8 not a supported charset");
            }

            Files.createParentDirs(file);
            String data = yaml.saveToString();

            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), cs)) {
                writer.write(data);
            }
        }
    }

    // retrieves the version of our database format, -1 if it doesn't exist
    // 1 = v1.0.0
    int getDatabaseVersion() {
        if (mdb == null) {
            return -1;
        }
        return mdb.getInt("courierdatabaseversion", -1);
    }

    void setDatabaseVersion(int v) {
        if (mdb == null) {
            return;
        }
        mdb.set("courierdatabaseversion", v);
    }

    // retrieves what we think is our specially allocated Map
    public int getCourierMapId() {
        if (mdb == null) {
            return -1;
        }
        return mdb.getInt("courierclaimedmap", -1);
    }
    
    void setCourierMapId(int mapId) {
        if (mdb == null) {
            return;
        }
        mdb.set("courierclaimedmap", mapId);
    }

    public boolean sendMessage(int id, String recipient, String sender) {
        boolean ret = false;
        if (mdb == null || recipient == null || sender == null) {
            return false;
        }

        recipient = recipient.toLowerCase();
        
        // nothing to say the player who wants to send a picked up Letter is the one with it in her storage
        // but if player2 steals a letter written by player1 and immediately sends to player3, player1
        // should not be listed as sender. See outcommented getSender() below

        String origin = getPlayerByMessageId(id);

        if (origin != null) {
            // alright, sign over to a specific receiver
            // String s = getSender(origin, id);
            String m = getMessage(origin, id);
            int date = getDate(origin, id);

            List<Integer> messageIdList = mdb.getIntegerList(recipient + KEY_MESSAGEID_SUFFIX);
            if(!messageIdList.contains(id)) { // I should move to a non-duplicate storage type ..
                messageIdList.add(id);
            }
            mdb.set(recipient + KEY_MESSAGEID_SUFFIX, messageIdList);
            mdb.set(recipient + "." + id + KEY_SENDER_SUFFIX, sender);
            mdb.set(recipient + "." + id + KEY_MESSAGE_SUFFIX, m);
            mdb.set(recipient + "." + id + KEY_DATE_SUFFIX, date);
            // new messages can't have been delivered
            mdb.set(recipient + "." + id + KEY_DELIVERED_SUFFIX, false);
            // new messages can't have been read
            mdb.set(recipient + "." + id + KEY_READ_SUFFIX, false);

            // since there's at least one new message, set newmail to true
            mdb.set(recipient + KEY_NEWMAIL_SUFFIX, true);

            // if we send to ourselves, don't delete what we just added
            if (!recipient.equalsIgnoreCase(origin)) {
                // "atomic" remove
                messageIdList = mdb.getIntegerList(origin + KEY_MESSAGEID_SUFFIX);
                // safety check
                messageIdList.remove(Integer.valueOf(id));
                mdb.set(origin + KEY_MESSAGEID_SUFFIX, messageIdList);
                mdb.set(origin + "." + id, null);
            }

            this.save(null);
            ret = true;
        }
        return ret;    
    }

    public boolean storeMessage(int id, String sender, String message, int date) {
        if (mdb == null || sender == null || message == null) {
            return false;
        }

        String senderKey = sender.toLowerCase();
        String origin = getPlayerByMessageId(id);

        // update messageids
        List<Integer> messageids = mdb.getIntegerList(senderKey + KEY_MESSAGEID_SUFFIX);
        if (!messageids.contains(id)) { // I should move to a non-duplicate storage type ..
            messageids.add(id);
        }
        mdb.set(senderKey + KEY_MESSAGEID_SUFFIX, messageids);

        mdb.set(senderKey + "." + id + KEY_SENDER_SUFFIX, sender);
        mdb.set(senderKey + "." + id + KEY_MESSAGE_SUFFIX, message);
        mdb.set(senderKey + "." + id + KEY_DATE_SUFFIX, date);
        mdb.set(senderKey + "." + id + KEY_DELIVERED_SUFFIX, true);
        mdb.set(senderKey + "." + id + KEY_READ_SUFFIX, true);
        // we do not change .newmail when storing in our own storage, of course

        if (origin != null && !sender.equalsIgnoreCase(origin)) {
            // the current writer of this letter was not the same as the last, make sure it's moved
            messageids = mdb.getIntegerList(origin + KEY_MESSAGEID_SUFFIX);
            messageids.remove(Integer.valueOf(id));
            mdb.set(origin + KEY_MESSAGEID_SUFFIX, messageids);
            mdb.set(origin + "." + id, null);
        }
        
        this.save(null); // save after each stored message currently

        return true;
    }

    // this method is called when we detect a database version with case sensitive keys
    // it simply lowercases all Player name keys
    void keysToLower() {
        if (mdb == null) {
            return;
        }

        // just for safety, back up db first, and don't allow the backup to be overwritten if it exists
        // (if this method throws exceptions most admins will just likely try a few times .. )
        String backup = FILENAME + ".100.backup";
        File db = new File(plugin.getDataFolder(), backup);
        if (!db.exists()) {
            this.save(backup);
        }
        
        Set<String> players = mdb.getKeys(false);
        for (String r : players) {
            String rlower = r.toLowerCase();

            if (!r.equals(rlower)) {
                // this receiver needs full rewriting
                boolean newmail = mdb.getBoolean(r + KEY_NEWMAIL_SUFFIX);
                List<Integer> messageIDs = mdb.getIntegerList(r + KEY_MESSAGEID_SUFFIX);
                List<Integer> newMessageIDs = mdb.getIntegerList(rlower + KEY_MESSAGEID_SUFFIX);
                for (Integer id : messageIDs) {
                    // fetch a message
                    String s = mdb.getString(r + "." + id + KEY_SENDER_SUFFIX);
                    String m = mdb.getString(r + "." + id + KEY_MESSAGE_SUFFIX);
                    int date = mdb.getInt(r + "." + id + KEY_DATE_SUFFIX);
                    boolean delivered = mdb.getBoolean(r + "." + id + KEY_DELIVERED_SUFFIX);
                    boolean read = mdb.getBoolean(r + "." + id + KEY_READ_SUFFIX);
                    
                    mdb.set(rlower + "." + id + KEY_SENDER_SUFFIX, s);
                    mdb.set(rlower + "." + id + KEY_MESSAGE_SUFFIX, m);
                    mdb.set(rlower + "." + id + KEY_DATE_SUFFIX, date);
                    mdb.set(rlower + "." + id + KEY_DELIVERED_SUFFIX, delivered);
                    mdb.set(rlower + "." + id + KEY_READ_SUFFIX, read);

                    newMessageIDs.add(id);

                    mdb.set(r + "." + id, null); // delete old message
                }
                mdb.set(rlower + KEY_MESSAGEID_SUFFIX, newMessageIDs);
                mdb.set(rlower + KEY_NEWMAIL_SUFFIX, newmail);

                mdb.set(r, null); // delete the old entry
            }
        }
        this.save(null);
    }
    
    // used for legacy Letter conversion only
    public boolean storeDate(int id, int d) {
        if (mdb == null) {
            return false;
        }
        
        String player = getPlayerByMessageId(id);
        if (player == null) {
            return false; // this would be bad
        }
        mdb.set(player + "." + id + KEY_DATE_SUFFIX, d);

        return true;
    }
    
    // currently used for legacy Letter conversion only, but it is generalized
    public void changeId(int oldid, int newid) {
        if (mdb == null) {
            return;
        }
        
        String r = getPlayerByMessageId(oldid);
        String s = getSender(r, oldid);
        String m = getMessage(r, oldid);
        int date = getDate(r, oldid);
        boolean delivered = getDelivered(r, oldid);
        boolean read = getRead(r, oldid);
        
        List<Integer> messageIDs = mdb.getIntegerList(r + KEY_MESSAGEID_SUFFIX);
        messageIDs.add(newid);
        // "atomic" add
        mdb.set(r + KEY_MESSAGEID_SUFFIX, messageIDs);
        mdb.set(r + "." + newid + KEY_SENDER_SUFFIX, s);
        mdb.set(r + "." + newid + KEY_MESSAGE_SUFFIX, m);
        mdb.set(r + "." + newid + KEY_DATE_SUFFIX, date);
        mdb.set(r + "." + newid + KEY_DELIVERED_SUFFIX, delivered);
        mdb.set(r + "." + newid + KEY_READ_SUFFIX, read);

        // "atomic" remove
        messageIDs.remove(Integer.valueOf(oldid)); // caught out by ArrayList.remove(Object o) vs remove(int i) ...
        mdb.set(r + KEY_MESSAGEID_SUFFIX, messageIDs);
        mdb.set(r + "." + oldid, null);
    }

    public boolean undeliveredMail(String recipient) {
        //noinspection SimplifiableIfStatement
        if (mdb == null || recipient == null) {
            return false;
        }
        
        recipient = recipient.toLowerCase();

        return mdb.getBoolean(recipient + KEY_NEWMAIL_SUFFIX);
    }

    // runs through messageids, sets all unread messages to undelivered
    // returns false when there are no unread messages
    public boolean deliverUnreadMessages(String r) {
        if (mdb == null || r == null) {
            return false;
        }

        r = r.toLowerCase();

        boolean newMail = false;
        List<Integer> messageids = mdb.getIntegerList(r + KEY_MESSAGEID_SUFFIX);
        for (Integer id : messageids) {
            boolean read = mdb.getBoolean(r + "." + id + KEY_READ_SUFFIX);
            if (!read) {
                mdb.set(r + "." + id + KEY_DELIVERED_SUFFIX, false);
                newMail = true;
            }
        }
        if (newMail) {
            mdb.set(r + KEY_NEWMAIL_SUFFIX, newMail);
        }
        return newMail;
    }

    // runs through messageids, finds a message not read and returns the corresponding id
    // returns -1 on failure
    public int unreadMessageId(String r) {
        if (mdb == null || r == null) {
            return -1;
        }

        r = r.toLowerCase();

        List<Integer> messageids = mdb.getIntegerList(r + KEY_MESSAGEID_SUFFIX);
        for (Integer id : messageids) {
            boolean read = mdb.getBoolean(r + "." + id + KEY_READ_SUFFIX);
            if (!read) {
                return id;
            }
        }
        return -1;
    }

    // runs through messageids, finds a message not delivered and returns the corresponding id
    // returns -1 on failure
    public int undeliveredMessageId(String recipient) {
        if (mdb == null || recipient == null) {
            return -1;
        }

        recipient = recipient.toLowerCase();

        List<Integer> messageids = mdb.getIntegerList(recipient + KEY_MESSAGEID_SUFFIX);
        for (Integer id : messageids) {
            boolean delivered = mdb.getBoolean(recipient + "." + id + KEY_DELIVERED_SUFFIX);
            if (!delivered) {
                return id;
            }
        }

        // if we end up here, for any reason, it means there are no undelivered messages
        mdb.set(recipient + KEY_NEWMAIL_SUFFIX, false);
        return -1;
    }

    // removes a single Letter from the database
    public boolean deleteMessage(short id) {
        if (id == -1 || mdb == null) {
            return false;
        }

        String r = getPlayerByMessageId(id);
        if (r == null) {
            return false;
        }

        List<Integer> messageIdList = mdb.getIntegerList(r + KEY_MESSAGEID_SUFFIX);

        // "atomic" remove
        messageIdList.remove(Integer.valueOf(id)); // caught out by ArrayList.remove(Object o) vs remove(int i) ...
        mdb.set(r + KEY_MESSAGEID_SUFFIX, messageIdList);
        mdb.set(r + "." + id, null);

        // todo: If our Letter had been delivered to another Player then remove that delivered info for them too.
        // seems not critical. new letters will set delivered to false even if ID is reused

        return true;
    }

    // does this id exist in the database
    // todo: horribly inefficient compared to just calling getPlayer() - due to using YAML instead of SQLite
    //       in this mergeback from the v1.2.0 branch
    boolean isValid(int id) {
        if (id == -1 || mdb == null) {
            return false;
        }

        Set<String> strings = mdb.getKeys(false);
        for (String key : strings) {
            List<Integer> messageIdList = mdb.getIntegerList(key + KEY_MESSAGEID_SUFFIX);
            if (messageIdList.contains(id)) {
                return true;
            }
        }

        return false;
    }

    // finds a specific messageid and returns associated player
    String getPlayerByMessageId(int id) {
        if (id == -1 || mdb == null) {
            return null;
        }
        
        Set<String> strings = mdb.getKeys(false);
        for (String key : strings) {
            List<Integer> messageIdList = mdb.getIntegerList(key + KEY_MESSAGEID_SUFFIX);
            if (messageIdList.contains(id)) {
                return key;
            }
        }
        return null;
    }
    
    public String getSender(String recipient, int id) {
        if (mdb == null || recipient == null) {
            return null;
        }

        return mdb.getString(recipient.toLowerCase() + "." + id + KEY_SENDER_SUFFIX);
    }
    
    public String getMessage(String recipient, int id) {
        if (mdb == null || recipient == null) {
            return null;
        }

        return mdb.getString(recipient.toLowerCase() + "." + id + KEY_MESSAGE_SUFFIX);
    }

    private boolean getDelivered(String r, int id) {
        //noinspection SimplifiableIfStatement
        if (mdb == null || r == null || id==-1) {
            return false;
        }

        r = r.toLowerCase();

        return mdb.getBoolean(r + "." + id + KEY_DELIVERED_SUFFIX);
    }

    // unexpected side effect, we end up here if player1 takes a message intended for player2
    // exploit or remove logging of it?
    public boolean setDelivered(String recipient, int id) {
        if (mdb == null || recipient == null || id==-1) {
            return false;
        }

        recipient = recipient.toLowerCase();

        mdb.set(recipient + "." + id + KEY_DELIVERED_SUFFIX, true);
        undeliveredMessageId(recipient); // DIRTY way of making sure "newmail" is cleared
        return true;
    }

    int getDate(String r, int id) {
        if (mdb == null || r == null) {
            return -1;
        }

        r = r.toLowerCase();

        return mdb.getInt(r + "." + id + KEY_DATE_SUFFIX);
    }

    boolean getRead(String r, int id) {
        //noinspection SimplifiableIfStatement
        if (mdb == null || r == null || id==-1) {
            return false;
        }

        r = r.toLowerCase();

        return mdb.getBoolean(r + "." + id + KEY_READ_SUFFIX);
    }

    public boolean setRead(String reader, int id) {
        if (mdb == null || reader == null || id==-1) {
            return false;
        }

        reader = reader.toLowerCase();

        mdb.set(reader + "." + id + KEY_READ_SUFFIX, true);
        return true;
    }

    // returns the first available id, or -1 when we're fatally out of them (or db error .. hmm)
    // expected to be called seldom (at letter creation) and is allowed to be slow
    // obvious caching/persisting of TreeSet possible
    public int generateUID() {
        if (mdb == null) {
            return -1;
        }
        TreeSet<Integer> sortedSet = new TreeSet<>();
        Set<String> players = mdb.getKeys(false);
        for (String player : players) {
            List<Integer> messageIDs = mdb.getIntegerList(player + KEY_MESSAGEID_SUFFIX);
            // add all messageids found for this player to our ordered set
            sortedSet.addAll(messageIDs);
        }
        // make sure we don't enter negative number territory
        // todo: introduce "fuzziness" making nextId less predictable
        for (int i=Courier.MIN_ID; i<Courier.MAX_ID; i++) {
            if (sortedSet.add(i)) {
                // i wasn't in the set
                return i;
            }
        }
        return -1;
    }
}
