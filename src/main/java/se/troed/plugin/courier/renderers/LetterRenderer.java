package se.troed.plugin.courier.renderers;

import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MapRenderer;
import org.jetbrains.annotations.NotNull;
import se.troed.plugin.courier.Courier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

public abstract class LetterRenderer extends MapRenderer {

	@SuppressWarnings("FieldCanBeLocal")
	protected static final int HEADER_POS = 2; // 2*getHeight()
	@SuppressWarnings("FieldCanBeLocal")
	protected static final int BODY_POS = 4; // 4*getHeight()
	@SuppressWarnings("FieldCanBeLocal")
	protected static final int CANVAS_WIDTH = 128;
	@SuppressWarnings("FieldCanBeLocal")
	protected static final int CANVAS_HEIGHT = 128;
	protected final Courier plugin;

	protected static byte[] clearImage = null; // singleton for

	protected LetterRenderer(boolean contextual, Courier p) {
		super(contextual);
		plugin = p;

		if (clearImage == null) {
			BufferedImage framed = null;
			try {
				InputStream is = plugin.getClass().getResourceAsStream("framed.png");
				if (is != null) {
					framed = ImageIO.read(is);
					is.close();
				}
			} catch (IOException e) {
				plugin.getCConfig().clog(Level.WARNING, "Unable to find framed.png in .jar");
				e.printStackTrace();
			}
			if (framed != null) {
				plugin.getCConfig().clog(Level.FINE, "framed.png found and read");
				clearImage = MapPalette.imageToBytes(framed);
			} else {
				clearImage = new byte[128 * 128];
				plugin.getCConfig().clog(Level.FINE, "framed.png not read");
			}
		}
	}

	// horizontal lines
	protected static void drawLine(MapCanvas canvas, int x, int y, int x2, byte c) {
		for (int t = x; t <= x2; t++) {
			canvas.setPixel(t, y, c);
		}
	}

	protected static void clearCanvas(@NotNull MapCanvas canvas) {
		for (int j = 0; j < CANVAS_HEIGHT; j++) {
			for (int i = 0; i < CANVAS_WIDTH; i++) {
				// canvas.setPixel(i, j, clearImage[j * 128 + i]) // use an image file
				canvas.setPixel(i, j, MapPalette.LIGHT_BROWN); // alternative: MapPalette.TRANSPARENT
			}
		}
	}
}
