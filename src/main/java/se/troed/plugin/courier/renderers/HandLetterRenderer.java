package se.troed.plugin.courier.renderers;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;
import org.jetbrains.annotations.NotNull;
import se.troed.plugin.courier.Courier;
import se.troed.plugin.courier.Letter;
import se.troed.plugin.courier.events.CourierReadEvent;

import java.util.logging.Level;

public class HandLetterRenderer extends LetterRenderer {

    private String cachedPrivacy;
    private String cachedReceiver = "";

    public HandLetterRenderer(Courier p) {
        super(true, p); // all our messages are contextual (i.e. different for different players)
    }

    // This method gets called at 20tps whenever a map is in a players inventory. Bail out as quickly as possible if we
    // shouldn't do anything with it.
    // https://bukkit.atlassian.net/browse/BUKKIT-476
    @Override
    public void render(@NotNull MapView map, @NotNull MapCanvas canvas, Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        if(plugin.courierMapType(item) != Courier.NONE) {
            Letter letter = plugin.getTracker().getLetter(item);
            if(letter == null) { // plugin.courierMapType(item) != Courier.LETTER
                // parchment - drawn blank below
                // currently redraws at 20 tps - no letter to clear dirty flag
                clearCanvas(canvas);
            } else if(letter.getDirty()) {
                plugin.getCConfig().clog(Level.FINE, "Rendering a Courier Letter (" + letter.getId() + ") on Map (" + map.getId() + ")");
                clearCanvas(canvas);
                // todo: idea for pvp war servers: "your mail has fallen into enemy hands". "they've read it!")
                if(letter.isAllowedToSee(player)) {
                    int drawPos = HEADER_POS;
                    if(letter.getHeader() != null) {
                        canvas.drawText(0, MinecraftFont.Font.getHeight() * drawPos, MinecraftFont.Font, letter.getHeader());
                        drawLine(canvas, 10, MinecraftFont.Font.getHeight() * (drawPos+1) +
                                            (int)(MinecraftFont.Font.getHeight() * 0.4), CANVAS_WIDTH-11, MapPalette.DARK_BROWN);
                        drawPos = BODY_POS;
                    }

                    canvas.drawText(letter.getLeftMarkerPos(), MinecraftFont.Font.getHeight(), MinecraftFont.Font, letter.getLeftMarker());
                    canvas.drawText(letter.getRightMarkerPos(), MinecraftFont.Font.getHeight(), MinecraftFont.Font, letter.getRightMarker());

                    if(letter.getMessage() != null) {
                        canvas.drawText(0,
                                    MinecraftFont.Font.getHeight() * drawPos,
                                    MinecraftFont.Font, Letter.MESSAGE_COLOR + letter.getMessage());
                    }

                    if(letter.getDisplayDate() != null) {
                        canvas.drawText(letter.getDisplayDatePos(),
                                        0,
                                        MinecraftFont.Font, Letter.DATE_COLOR + letter.getDisplayDate());
                    }

                    // this is the actual time we can be sure a letter has been read
                    // post an event to make sure we don't block the rendering pipeline
                    if(!letter.getRead()) {
                        CourierReadEvent event = new CourierReadEvent(player, letter.getId());
                        plugin.getServer().getPluginManager().callEvent(event);
                        letter.setRead(true);
                    }
                } else {
                    if(!letter.getReceiver().equalsIgnoreCase(cachedReceiver)) {
                        cachedReceiver = letter.getReceiver();
                        cachedPrivacy = plugin.getCConfig().getPrivacyLocked(cachedReceiver);
                    }
                    canvas.drawText(0, MinecraftFont.Font.getHeight()*HEADER_POS, MinecraftFont.Font, cachedPrivacy);
                }
                letter.setDirty(false);
                player.sendMap(map);
            }
        }
    }
}
