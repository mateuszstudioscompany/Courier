package se.troed.plugin.courier.postmen;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import se.troed.plugin.courier.Courier;

/* Allows all Creatures to be Postmen, although we only test with Villagers (and Enderman)
 *
 * There is an issue with Creatures "pushing" the player, possibly into lava. I would need
 * https://bukkit.atlassian.net/browse/BUKKIT-127 (onEntityMove) to solve that, or
 * I could refrain from doing .setTarget() of course - see getWalkToPlayer()
 *
 */
public class CreaturePostman extends Postman {

    CreaturePostman(Courier plug, Player p, int id, EntityType t) {
        super(plug, p, id, t);
    }

    @Override
    public void spawn(Location l) {
        super.spawn(l);
        if(plugin.getCConfig().getWalkToPlayer()) {
            postmanEntity.setTarget(player);
        }
        EntityEquipment equipment = postmanEntity.getEquipment();
        if (equipment != null) {
            equipment.setItemInMainHand(new ItemStack(Material.PAPER));
        }
    }

    @Override
    public void drop() {
        if(plugin.getCConfig().getWalkToPlayer()) {
            postmanEntity.setTarget(null);
        }
        EntityEquipment equipment = postmanEntity.getEquipment();
        if (equipment != null) {
            equipment.setItemInMainHand(new ItemStack(Material.PAPER));
        }
        super.drop();
    }
}
