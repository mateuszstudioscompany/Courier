package se.troed.plugin.courier.postmen;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import se.troed.plugin.courier.Courier;

public class EnderPostman extends Postman {

    EnderPostman(Courier plug, Player p, int id) {
        super(plug, p, id, EntityType.ENDERMAN);
    }

    @Override
    public void spawn(Location l) {
        super.spawn(l);
        ((Enderman) postmanEntity).setCarriedBlock(Material.BOOKSHELF.createBlockData());
    }

    @Override
    public void drop() {
        ((Enderman) postmanEntity).setCarriedBlock(Material.AIR.createBlockData());
        super.drop();
    }
}
