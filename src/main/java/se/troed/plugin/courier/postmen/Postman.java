package se.troed.plugin.courier.postmen;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;
import se.troed.plugin.courier.Courier;

import java.util.UUID;

/**
 * A Postman is a friendly Creature, tirelessly carrying around our mail
 *
 * One will be spawned for each Player that will receive mail
 */
public abstract class Postman {

    protected Creature postmanEntity;
    protected EntityType type;
    protected final Courier plugin;
    protected final Player player;
    protected final ItemStack letterItem;
    protected UUID uuid;
    protected boolean scheduledForQuickRemoval;
    protected int taskId;
    protected Runnable runnable;

    protected Postman(Courier plug, Player p, int id, EntityType t) {
        plugin = plug;
        player = p;
        type = t;
        // Postmen, like players doing /letter, can create actual Items
        letterItem = new ItemStack(Material.FILLED_MAP, 1);
        letterItem.addUnsafeEnchantment(Enchantment.DURABILITY, id);
        MapMeta letterMeta = (MapMeta) letterItem.getItemMeta();
        letterMeta.setMapId(plug.getCourierDB().getCourierMapId());
        letterItem.setItemMeta(letterMeta);
    }
    
    public static Postman create(Courier plug, Player p, int id) {
        if(plug.getCConfig().getPostmanEntityType() == EntityType.ENDERMAN) {
            return new EnderPostman(plug, p, id);
        } else {
            return new CreaturePostman(plug, p, id, plug.getCConfig().getPostmanEntityType());
        }
    }

    // must be implemented
    public void spawn(Location l) {
        postmanEntity = (Creature) player.getWorld().spawnEntity(l, type);
        uuid = postmanEntity.getUniqueId();
        postmanEntity.setCustomName("Postman");
        postmanEntity.setCustomNameVisible(true);
    }

    public EntityType getType() {
        return type;
    }

    // yes I know this fails in many cases, we only "promise" Endermen and Villagers for now
    // would need to contain all Creatures for this to work reliably
    public static int getHeight(Courier plug) {
        EntityType type = plug.getCConfig().getPostmanEntityType();
        if(type == EntityType.ENDERMAN) {
            return 3;
        } else if(type == EntityType.VILLAGER ||
                  type == EntityType.BLAZE ||
                  type == EntityType.COW ||
                  type == EntityType.CREEPER ||
                  type == EntityType.MUSHROOM_COW ||
                  type == EntityType.ZOMBIFIED_PIGLIN ||
                  type == EntityType.SHEEP ||
                  type == EntityType.SKELETON ||
                  type == EntityType.SNOWMAN ||
                  type == EntityType.SQUID ||
                  type == EntityType.ZOMBIE) {
            return 2;
        } else {
            return 1;
        }
    }

    public ItemStack getLetterItem() {
        return letterItem;
    }

    public void cannotDeliver() {
        Courier.display(player, plugin.getCConfig().getCannotDeliver());
    }

    public void announce(Location l) {
        // todo: if in config, play effect
        player.playEffect(l, Effect.BOW_FIRE, 100);
        Courier.display(player, plugin.getCConfig().getGreeting());
    }
    
    public void drop() {
        postmanEntity.getWorld().dropItemNaturally(postmanEntity.getLocation(), letterItem);
        Courier.display(player, plugin.getCConfig().getMailDrop());
    }

    public UUID getUUID() {
        return uuid;
    }

    public void remove() {
        postmanEntity.remove();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean scheduledForQuickRemoval() {
        return scheduledForQuickRemoval;
    }
    
    public void setTaskId(int t) {
        taskId = t;
    }
    
    public int getTaskId() {
        return taskId;
    }

    public void setRunnable(Runnable r) {
        runnable = r;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    // Called when either mail has been delivered or someone is attacking the postman
    public void quickDespawn() {
        plugin.getTracker().schedulePostmanDespawn(this.uuid, plugin.getCConfig().getQuickDespawnTime());
        scheduledForQuickRemoval = true;
    }
}